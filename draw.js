// include all glMatrix classes
const mat2 = glMatrix.mat2;
const mat2d = glMatrix.mat2d;
const mat3 = glMatrix.mat3;
const mat4 = glMatrix.mat4;

const quat = glMatrix.quat;
const quat2 = glMatrix.quat2;

const vec2 = glMatrix.vec2;
const vec3 = glMatrix.vec3;
const vec4 = glMatrix.vec4;

/*
    Initializing the web GL version 2 context for advanced graphics.
*/
const canvas = document.getElementById("glCanvas");
/** @type {WebGL2RenderingContext} */
const gl = canvas.getContext("webgl2");
if (!gl) {
  alert(
    "Unable to initialize WebGL. Your browser or machine may not support it."
  );
}

/**
 * Shader helper class to load and complier shader from a source code.
 */
class Shader {
  #vertexShaderSource;
  #fragmentShaderSource;
  #program;
  #gl;

  constructor(gl, vsSource, fsSource) {
    this.#vertexShaderSource = vsSource;
    this.#fragmentShaderSource = fsSource;
    this.#gl = gl;
    this.#program = this.initShaderProgram(
      this.#vertexShaderSource,
      this.#fragmentShaderSource
    );
  }

  use() {
    this.#gl.useProgram(this.#program);
  }

  setInt(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    this.#gl.uniform1i(uniformID, value);
  }

  setFloat(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    this.#gl.uniform1f(uniformID, value);
  }

  setVec3(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    this.#gl.uniform3fv(uniformID, value);
  }

  setVec4(uniformName, value) {
    const uniformID = gl.getUniformLocation(this.#program, uniformName);
    this.#gl.uniform4fv(uniformID, value);
  }

  setMat4(uniformName, value) {
    const uniformID = this.#gl.getUniformLocation(this.#program, uniformName);
    this.#gl.uniformMatrix4fv(uniformID, false, value);
  }

  getUniformLocation(uniformName) {
    return this.#gl.getUniformLocation(this.#program, uniformName);
  }

  getShaderSource(url) {
    let req = new XMLHttpRequest();
    req.open("GET", url, false);
    req.send();
    return req.status === 200 ? req.responseText : null;
  }

  loadShader(type, source) {
    const shader = this.#gl.createShader(type);
    this.#gl.shaderSource(shader, source);
    this.#gl.compileShader(shader);

    if (!this.#gl.getShaderParameter(shader, this.#gl.COMPILE_STATUS)) {
      alert(
        "An error occurred compiling the shaders: " +
          this.#gl.getShaderInfoLog(shader)
      );
      this.#gl.deleteShader(shader);
      return null;
    }
    return shader;
  }

  initShaderProgram(vsSource, fsSource) {
    const vertexShader = this.loadShader(gl.VERTEX_SHADER, vsSource);
    const fragmentShader = this.loadShader(gl.FRAGMENT_SHADER, fsSource);

    const shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      alert(
        "Unable to initialize the shader program: " +
          gl.getProgramInfoLog(shaderProgram)
      );
      return null;
    }

    return shaderProgram;
  }
}

/**
 * Baisc building block for 3D shapes, Vertex class.
 */
class Vertex {
  constructor(position, normal) {
    this._position = position;
    this._normal = normal;
  }

  get position() {
    return this._position;
  }

  get normal() {
    return this._normal;
  }
}

/**
 * Helper class for complex 3D shapes.
 */
class Entity {
  _Transformation;
  constructor() {
    this._Transformation = mat4.create();
  }

  translate(direction) {
    mat4.translate(this._Transformation, this._Transformation, direction);
  }

  resetTransform() {
    this._Transformation = mat4.create();
  }

  rotate(rotation) {
    const quaternion = quat.create();
    const rot = mat4.create();
    quat.fromEuler(quaternion, rotation[0], rotation[1], rotation[2]);
    mat4.fromQuat(rot, quaternion);
    mat4.multiply(this._Transformation, this._Transformation, rot);
  }

  scale(scale) {
    mat4.scale(this._Transformation, this._Transformation, scale);
  }

  uniformscale(factor) {
    this.scale([factor, factor, factor]);
  }

  transform(scale, rotation, translation) {
    this.scale(scale);
    this.rotate(rotation);
    this.translate(translation);
  }

  get Transformation() {
    return this._Transformation;
  }

  set Position(position) {
    this._Transformation[12] = position[0];
    this._Transformation[13] = position[1];
    this._Transformation[14] = position[2];
  }

  get Position() {
    let position = vec3.create();
    mat4.getTranslation(position, this._Transformation);
    return position;
  }

  get Rotation() {
    let rotation = quat.create();
    mat4.getRotation(rotation, this._Transformation);
    return rotation;
  }

  get Scale() {
    let scale = vec3.create();
    mat4.getScaling(scale, this._Transformation);
    return scale;
  }
}

/**
 * Extended class representing a point light source.
 * A helper class to control the light source.
 */
class PointLight extends Entity {
  #color;
  #strength;

  constructor(color, strength) {
    super();
    this.#color = color;
    this.#strength = strength;
  }

  set Color(color) {
    this.#color = color;
  }

  get Color() {
    return this.#color;
  }

  set Strength(strength) {
    this.#strength = strength;
  }

  get Strength() {
    return this.#strength;
  }
}

class Material {
  #shader;
  #baseColor;
  #gl;

  constructor(glContext, shader, color) {
    this.#gl = glContext;
    this.#shader = shader;
    this.#baseColor = color;
  }

  loadTexture(texture, path) {
    const image = new Image();
    image.onload = function () {
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this);
      gl.generateMipmap(gl.TEXTURE_2D);
    };
    image.src = path;
  }

  get Shader() {
    return this.#shader;
  }

  get BaseColor() {
    return this.#baseColor;
  }
}

class PBRMaterial extends Material {
  #colorMap;
  #metalMap;
  #roughnessMap;
  #normalMap;
  #gl;

  constructor(
    glContext,
    color,
    colorMapPath,
    metalMapPath,
    roughnessMapPath,
    normalMapPath,
    shader
  ) {
    super(glContext, shader, color);
    this.#gl = glContext;

    this.#colorMap = this.#gl.createTexture();
    this.#metalMap = this.#gl.createTexture();
    this.#roughnessMap = this.#gl.createTexture();
    this.#normalMap = this.#gl.createTexture();

    this.loadTexture(this.#colorMap, colorMapPath);
    this.loadTexture(this.#metalMap, metalMapPath);
    this.loadTexture(this.#roughnessMap, roughnessMapPath);
    this.loadTexture(this.#normalMap, normalMapPath);
  }

  get ColorMap() {
    return this.#colorMap;
  }

  get MetalMap() {
    return this.#metalMap;
  }

  get RoughnessMap() {
    return this.#roughnessMap;
  }

  get NormalMap() {
    return this.#normalMap;
  }
}

/**
 * Extended class to create a 3D shape, extends Entity.
 * Helper class to draw the 3D shaper using the selected shader.
 */
class Mesh extends Entity {
  #vao;
  #vbo;
  #indexBuffer;
  #indices;
  #color;
  #shader;
  #material;
  #camera;

  constructor(shader, color, material, camera) {
    super();
    this.#shader = shader;
    this.#color = color;
    this.#material = material;
    this.#camera = camera;
  }

  get Shader() {
    return this.#shader;
  }

  set Shader(shader) {
    this.#shader = shader;
  }

  setupMesh(vertices, indices) {
    this.#indices = indices;
    this.#vao = gl.createVertexArray();
    this.#vbo = gl.createBuffer();
    this.#indexBuffer = gl.createBuffer();

    // 3 x 4bytes for position, 4 x 4bytes for normal
    const bytesPerVertex = 28;

    const buffer = new ArrayBuffer(bytesPerVertex * vertices.length);
    const dataView = new DataView(buffer);

    for (let i = 0; i < vertices.length; i++) {
      // console.log(i);
      dataView.setFloat32(bytesPerVertex * i, vertices[i].position[0], true);
      dataView.setFloat32(
        bytesPerVertex * i + 4,
        vertices[i].position[1],
        true
      );
      dataView.setFloat32(
        bytesPerVertex * i + 8,
        vertices[i].position[2],
        true
      );

      dataView.setFloat32(bytesPerVertex * i + 12, vertices[i].normal[0], true);
      dataView.setFloat32(bytesPerVertex * i + 16, vertices[i].normal[1], true);
      dataView.setFloat32(bytesPerVertex * i + 20, vertices[i].normal[2], true);
      dataView.setFloat32(bytesPerVertex * i + 24, vertices[i].normal[3], true);
    }

    gl.bindVertexArray(this.#vao);

    gl.bindBuffer(gl.ARRAY_BUFFER, this.#vbo);
    gl.bufferData(gl.ARRAY_BUFFER, buffer, gl.STATIC_DRAW);

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.#indexBuffer);
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this.#indices, gl.STATIC_DRAW);

    // vertex positions
    gl.vertexAttribPointer(0, 3, gl.FLOAT, false, bytesPerVertex, 0);
    gl.enableVertexAttribArray(0);

    // vertex normals
    gl.enableVertexAttribArray(1);
    gl.vertexAttribPointer(1, 4, gl.FLOAT, false, bytesPerVertex, 12);

    gl.bindVertexArray(null);
  }

  draw() {
    const shader = this.#material.Shader;
    this.#shader.use();
    // gl.bindTexture(gl.TEXTURE_2D, this.#material.ColorMap);

    const colorLocation = shader.getUniformLocation("u_ColorMap");
    const metalLocation = shader.getUniformLocation("u_MetalMap");
    const roughnessLocation = shader.getUniformLocation("u_RoughnessMap");
    const normalLocation = shader.getUniformLocation("u_NormalMap");

    gl.uniform1i(colorLocation, 0);
    gl.uniform1i(metalLocation, 1);
    gl.uniform1i(roughnessLocation, 2);
    gl.uniform1i(normalLocation, 3);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, this.#material.ColorMap);
    gl.activeTexture(gl.TEXTURE1);
    gl.bindTexture(gl.TEXTURE_2D, this.#material.MetalMap);
    gl.activeTexture(gl.TEXTURE2);
    gl.bindTexture(gl.TEXTURE_2D, this.#material.RoughnessMap);
    gl.activeTexture(gl.TEXTURE3);
    gl.bindTexture(gl.TEXTURE_2D, this.#material.NormalMap);

    scene.Lights.forEach((light, index) => {
      shader.setVec3(`lights[${index}].color`, light.Color);
      shader.setFloat(`lights[${index}].strength`, light.Strength);
    });

    shader.setMat4("uProjection", this.#camera.Projection);
    shader.setMat4("uView", this.#camera.InverseView);
    shader.setVec3("objectColor", this.#material.BaseColor);
    shader.setVec3("ambientColor", scene.AmbientColor);
    shader.setInt("numberLights", scene.Lights.length);
    shader.setMat4("uWorld", this.Transformation);
    shader.setVec3("viewPosition", this.#camera.Position);

    gl.bindVertexArray(this.#vao);
    gl.drawElements(gl.TRIANGLES, this.#indices.length, gl.UNSIGNED_SHORT, 0);
    gl.bindVertexArray(null);
  }
}

/**
 * This class extends the 3D Mesh class to create a Torus shape.
 */
class Torus extends Mesh {
  #vertices;
  #indices;
  #vertexData;
  constructor(resolution_n, resolution_m, shader, color, material, camera) {
    super(shader, color, material, camera);
    this.#vertexData = this.createVertexData(resolution_n, resolution_m);
    this.#vertices = this.#vertexData.vertices;
    this.#indices = this.#vertexData.indices;
    this.setupMesh(this.#vertices, this.#indices);
  }

  createVertexData(resolution_n, resolution_m) {
    var n = resolution_n;
    var m = resolution_m;

    // Positions.
    this.positions = new Float32Array(3 * (n + 1) * (m + 1));
    var positions = this.positions;
    // Normals.
    this.normals = new Float32Array(3 * (n + 1) * (m + 1));
    var normals = this.normals;
    // Index data.
    this.indicesLines = new Uint16Array(2 * 2 * n * m);
    var indicesLines = this.indicesLines;
    this.indicesTris = new Uint16Array(3 * 2 * n * m);
    var indicesTris = this.indicesTris;

    var du = (2 * Math.PI) / n;
    var dv = (2 * Math.PI) / m;
    var r = 0.15;
    var R = 0.5;
    // Counter for entries in index array.
    var iLines = 0;
    var iTris = 0;

    let vertices = [];
    // Loop angle u.
    for (var i = 0, u = 0; i <= n; i++, u += du) {
      // Loop angle v.
      for (var j = 0, v = 0; j <= m; j++, v += dv) {
        var iVertex = i * (m + 1) + j;
        let position = [];
        let normal = [];

        var x = (R + r * Math.cos(u)) * Math.cos(v);
        var y = (R + r * Math.cos(u)) * Math.sin(v);
        var z = r * Math.sin(u);

        // Set vertex positions.
        positions[iVertex * 3] = x;
        positions[iVertex * 3 + 1] = y;
        positions[iVertex * 3 + 2] = z;

        position.push(x);
        position.push(y);
        position.push(z);

        // Calc and set normals.
        var nx = Math.cos(u) * Math.cos(v);
        var ny = Math.cos(u) * Math.sin(v);
        var nz = Math.sin(u);

        normal.push(nx);
        normal.push(ny);
        normal.push(nz);
        normal.push(1.0);
        normals[iVertex * 3] = nx;
        normals[iVertex * 3 + 1] = ny;
        normals[iVertex * 3 + 2] = nz;

        // Set index.
        // Line on beam.
        if (j > 0 && i > 0) {
          indicesLines[iLines++] = iVertex - 1;
          indicesLines[iLines++] = iVertex;
        }
        // Line on ring.
        if (j > 0 && i > 0) {
          indicesLines[iLines++] = iVertex - (m + 1);
          indicesLines[iLines++] = iVertex;
        }

        // Set index.
        // Two Triangles.
        if (j > 0 && i > 0) {
          indicesTris[iTris++] = iVertex;
          indicesTris[iTris++] = iVertex - 1;
          indicesTris[iTris++] = iVertex - (m + 1);
          //
          indicesTris[iTris++] = iVertex - 1;
          indicesTris[iTris++] = iVertex - (m + 1) - 1;
          indicesTris[iTris++] = iVertex - (m + 1);
        }
        vertices.push(new Vertex(position, normal));
      }
    }
    return {
      vertices: vertices,
      indices: indicesTris,
    };
  }
}

/**
 * This class extends the 3D Mesh class to a helper class
 *  to load a 3D mesh out of an object file.
 */
class MeshLoader extends Mesh {
  #objSource;
  #vertices;
  #indices;
  #vertexData;
  constructor(path, shader, color) {
    super(shader, color);
    this.#objSource = this.loadOBJ(path);
    this.#vertexData = this.createVertexData(this.#objSource);
    this.#vertices = this.#vertexData.vertices;
    this.#indices = this.#vertexData.indices;
    this.setupMesh(this.#vertices, this.#indices);
  }

  loadOBJ(path) {
    const req = new XMLHttpRequest();
    req.open("GET", path, false);
    req.send();
    return req.status === 200 ? req.responseText : null;
  }

  createVertexData(objSource) {
    let lines = objSource.split(/\r?\n/);
    let positions = [];
    let normals = [];
    let faces = [];
    let texCoords = [];

    let vertices = [];

    lines.forEach((line) => {
      let attrib = line.split(" ")[0];
      let p = line.split(" ");
      if (attrib === "v") {
        let position = [parseFloat(p[1]), parseFloat(p[2]), parseFloat(p[3])];
        positions.push(position);
      } else if (attrib === "vt") {
        let texCoord = [parseFloat(p[1]), parseFloat(p[2])];
        texCoords.push(texCoord);
      } else if (attrib === "vn") {
        let normal = [parseFloat(p[1]), parseFloat(p[2]), parseFloat(p[3])];
        normals.push(normal);
      } else if (attrib === "f") {
        faces.push(p[1]);
        faces.push(p[2]);
        faces.push(p[3]);
      }
    });
    let indices = [];

    faces.forEach((face, index) => {
      let content = face.split("/");
      let positionIndex = parseInt(content[0]) - 1;
      let texCoordIndex = parseInt(content[1]) - 1;
      let normalIndex = parseInt(content[2]) - 1;
      let vertex = new Vertex(positions[positionIndex], normals[normalIndex]);
      vertices.push(vertex);
      indices.push(index);
    });

    let indicesTris = new Uint16Array(indices);

    return {
      vertices: vertices,
      indices: indicesTris,
    };
  }
}

/**
 * Helper class to create a camera view port and adjust camera
 * orientation and camera position and view.
 */
class Camera {
  #FOV;
  #zNear;
  #zFar;
  #position;
  #target;
  #aspectRatio;

  #projectionMatrix;
  #viewMatrix;

  constructor(resolution, FOV, zNear, zFar, position, target) {
    this.#aspectRatio = resolution[0] / resolution[1];
    this.#FOV = (FOV * Math.PI) / 180.0;
    this.#zNear = zNear;
    this.#zFar = zFar;
    this.#position = position;
    this.#target = target;

    this.#projectionMatrix = mat4.create();
    mat4.perspective(
      this.#projectionMatrix,
      this.#FOV,
      this.#aspectRatio,
      this.#zNear,
      this.#zFar
    );

    this.#viewMatrix = mat4.create();
    mat4.translate(this.#viewMatrix, this.#viewMatrix, this.#position);
    mat4.targetTo(
      this.#viewMatrix,
      this.#position,
      this.#target,
      [0.0, 1.0, 0.0]
    );
  }

  get Projection() {
    return this.#projectionMatrix;
  }

  get Position() {
    return this.#position;
  }

  get view() {
    return this.#viewMatrix;
  }

  get InverseView() {
    let inverse = mat4.create();
    mat4.invert(inverse, this.#viewMatrix);
    return inverse;
  }

  zoom(amount) {
    this.move([0, 0, -amount]);
  }

  move(direction) {
    mat4.translate(this.#viewMatrix, this.#viewMatrix, direction);
    mat4.getTranslation(this.#position, this.#viewMatrix);
    mat4.targetTo(
      this.#viewMatrix,
      this.#position,
      this.#target,
      [0.0, 1.0, 0.0]
    );
  }

  moveAlongCircle(angle) {
    let angle_rad = (angle * Math.PI) / 180;
    let destination = vec3.create();
    vec3.rotateY(destination, this.#position, [0.0, 0.0, 0.0], angle_rad);

    let direction = vec3.create();
    vec3.sub(direction, destination, this.#position);

    this.#viewMatrix[12] += direction[0];
    this.#viewMatrix[13] += direction[1];
    this.#viewMatrix[14] += direction[2];
    mat4.getTranslation(this.#position, this.#viewMatrix);
    mat4.targetTo(
      this.#viewMatrix,
      this.#position,
      this.#target,
      [0.0, 1.0, 0.0]
    );
  }
}

/**
 * Helper class to create scene using the camera view port, the selected
 * light source, and the 3D objects to be drawn.
 */
class Scene {
  #camera;
  #meshes;
  #lights;
  #ambientColor;

  constructor() {
    const resolution = [gl.canvas.clientWidth, gl.canvas.clientHeight];
    this.#camera = new Camera(resolution, 45, 0.1, 100, [0, 2, 7], [0, 2, 0]);
    this.#meshes = [];
    this.#lights = [];
    this.#ambientColor = [0, 0, 1];

    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
  }

  get camera() {
    return this.#camera;
  }

  get Lights() {
    return this.#lights;
  }

  get Meshes() {
    return this.#meshes;
  }

  get AmbientColor() {
    return this.#ambientColor;
  }

  addMesh(mesh) {
    this.#meshes.push(mesh);
  }

  addLight(light) {
    this.#lights.push(light);
  }

  draw() {
    this.#clear();
    this.#meshes.forEach((mesh) => {
      const shader = mesh.Shader;
      shader.use();

      this.#lights.forEach((light, index) => {
        shader.setVec3(`pointLights[${index}].position`, light.Position);
        shader.setVec3(`pointLights[${index}].color`, light.Color);
        shader.setFloat(`pointLights[${index}].strength`, light.Strength);
      });

      shader.setMat4("uProjection", this.#camera.Projection);
      shader.setMat4("uView", this.#camera.InverseView);
      shader.setVec3("ambientColor", [0.1, 0.2, 0.3]);
      shader.setInt("NumberLights", this.#lights.length);
      shader.setMat4("uWorld", mesh.Transformation);

      mesh.draw();
    });
  }

  #clear() {
    gl.clearColor(0.0, 0.0, 1, 1.0);
    gl.clearDepth(1.0);
    gl.clear(gl.COLOR_BUFFER_BIT || gl.DEPTH_BUFFER_BIT);
  }
}

/**
 * Implements a cylic rotation for the light source
 * @param {Current light source positon} position
 * @param {Required angle delta} angle
 * @param {Light source point vector} point
 * @returns
 */
function rotateAroundY(position, angle, point) {
  let angle_rad = (angle * Math.PI) / 180.0;
  let newPos = vec3.create();
  vec3.rotateY(newPos, position, point, angle_rad);
  return newPos;
}

// Physically Rendering Vertex Shader Source Code
const PRBVertexShader = `#version 300 es

layout (location = 0) in vec3 aVertexPosition;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoord;

uniform mat4 uView;
uniform mat4 uProjection;
uniform mat4 uWorld;


out vec3 VertexWorldPosition;
out vec3 Normal;
out vec2 TexCoord;

void main(){
  vec4 vertexPosition = vec4(aVertexPosition, 1.0);
  gl_Position = uProjection * uView * uWorld * vertexPosition;

  VertexWorldPosition = vec3(uWorld * vertexPosition);
  Normal = normalize(uWorld * vec4(aNormal, 0.0)).xyz;

  TexCoord = aTexCoord;
}`;

// Physically Rendering Fragment Shader Source Code
const PRBFragmentShader = `#version 300 es
precision highp float;


in vec3 VertexWorldPosition;
in vec3 Normal;
in vec2 TexCoord;

out vec4 FragColor;

struct Light
{
  int type;
  vec3 position;
  vec3 color;
  float strength;
};

#define MAX_NR_LIGHTS 20

uniform int numberLights;
uniform Light lights[MAX_NR_LIGHTS];
uniform vec3 objectColor;
uniform vec3 ambientColor;
uniform vec3 viewPosition;

uniform sampler2D u_texture;
float gamma = 2.2;
vec3 CalcLight(Light light, vec3 normal, vec3 fragPos, vec3 lightDir)
{
  float specularStrength = light.strength;
  vec3 viewDir = normalize(viewPosition - fragPos);
  vec3 reflectDir = reflect(-lightDir, normal);

  float diff = max(dot(normal, lightDir), 0.0);
  vec3 diffuse  = light.color  * diff * light.strength;

  float spec = pow(max(dot(viewDir, reflectDir), 0.0), 64.0);
  vec3 specular = specularStrength * spec * light.color;

  vec3 result = diffuse + specular;
  result = pow(result, vec3(1.0/gamma));
  return result;
}

vec3 GetPointLightDir(Light light, vec3 fragPos){
  return normalize(light.position - fragPos);
}

vec3 GetDirectionalLightDir(Light light){
  return normalize(light.position);
}

void main(){

  vec3 result = ambientColor;
  vec3 normal = normalize(Normal).xyz;

  vec3 lightDir = vec3(0);
  Light light;
  for(int i = 0; i < numberLights; i++)
  {
    light = lights[i];
    if(light.type == 0){
        lightDir = GetPointLightDir(light, VertexWorldPosition);
    }else if(light.type == 1){
      lightDir = GetDirectionalLightDir(light);
    }

    result += CalcLight(light, normal, VertexWorldPosition, lightDir);
  }

  result = result * objectColor * texture(u_texture, TexCoord).rgb;


  FragColor = vec4(result, 1.0);
}`;

const red = [1.0, 0.0, 0.0, 1.0];
const white = [1.0, 1.0, 1.0];
const lightGrey = [0.8, 0.8, 0.8, 1.0];
const darkGrey = [0.4, 0.4, 0.4, 1.0];

// Start scene
const scene = new Scene();

// Creating point light source 1
const light = new PointLight([1.0, 0.0, 0.0], 1.0);
scene.addLight(light);
light.Position = [0, 4, 4];

// Creating point light source 2
const light2 = new PointLight([0.0, 1.0, 0.0], 1.0);
scene.addLight(light2);
light2.Position = [4, 4, -4];

// Creating point light source 3
const light3 = new PointLight([0.0, 0.0, 1.0], 1.0);
scene.addLight(light3);
light3.Position = [-4, 4, -4];

// Creating physical rendering shader
const physicallyRenderingShader = new Shader(
  gl,
  PRBVertexShader,
  PRBFragmentShader
);

const torusMat = new PBRMaterial(
  gl,
  white,
  "color.png",
  "metal.png",
  "rough.png",
  "normal.png",
  physicallyRenderingShader
);

// Creating a Torus 3D object
const torus = new Torus(
  32,
  32,
  physicallyRenderingShader,
  white,
  torusMat,
  scene.camera
);
scene.addMesh(torus);
torus.rotate([0, 0, 0]);
torus.Position = [0, 2, 0];
torus.uniformscale(2);

let isToon = true;
let lightsRotating = true;
/**
 * Event handler for keyboard events
 * @param {Keyboard events} e
 */
function processInput(e) {
  switch (e.code) {
    case "KeyA":
      scene.camera.moveAlongCircle(-5);
      break;
    case "KeyD":
      scene.camera.moveAlongCircle(5);
      break;
    case "KeyS":
      scene.camera.move([0, -0.5, 0]);
      break;
    case "KeyW":
      scene.camera.move([0, 0.5, 0]);
      break;
    case "KeyI":
      scene.camera.zoom(0.5);
      break;
    case "KeyO":
      scene.camera.zoom(-0.5);
      break;
    case "KeyL":
      lightsRotating = !lightsRotating;
      break;
    default:
      break;
  }
}

// Load the event handler
window.addEventListener("keydown", processInput);

/**
 * Rotate the light source around the scene
 */
function draw() {
  if (lightsRotating) {
    scene.Lights.forEach((light) => {
      light.Position = rotateAroundY(light.Position, 0.2, [0, 0, 0]);
    });
  }

  scene.draw();
  window.requestIdleCallback(draw);
}

// Draw the scene on request
window.requestAnimationFrame(draw);
